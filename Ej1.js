let min = Number(prompt("¿Cuantos minutos has estado en el telefono este mes?"));
let tar = 10;

if (min > 180 && min <= 240) {
    dif = (min - 180);
    tar = (dif * 0.10) + tar;
}

else if (min > 240) {
    tar = 16;
    dif = (min - 240);
    tar = (dif * 0.20) + tar;
}

console.log("Tu tarifa es: "+tar);
