let jose = [1,6,8];

function standardDeviation(dataArray) {
    let suma = 0;

    for (let i = 0; i < dataArray.length; i++) {    
        suma += dataArray[i];

    }
    let media = suma / dataArray.length;

    let sumed = 0;
    let resta = 0;
    for (let i = 0; i < dataArray.length; i++) {
        resta = media - dataArray[i];
        sumed += Math.pow(resta, 2);

    }
    
    sumed = sumed / dataArray.length;
    let desvtip = Math.sqrt(sumed);
    console.log("La desviación típica de las cifras dadas es: ");
    console.log(desvtip);
}

standardDeviation(jose);